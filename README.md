# Interface Abstraction File System 0.1.0

Directory tree standard for abstraction internal and external interfaces

## Root location

The root of Interface Abstraction File System is according situations below:

### POSIX 

The preferred location of Interface Abstraction File System in POSIX systems is `/itf/`.
In alternative to this, can be in `/var/itf/`.

For non system wide location, can be in user directory, replacing slash for dot, like for example `/home/user/.itf/`.

The location of Interface Abstraction File System can be inside a specific program, product or other kind of organization direcotory. For example `/opt/program/itf/`, `/opt/program/`, `/home/user/.program/itf/` or `/home/user/.program/`.

For this specification, we will use preferred location of POSIX systems.

### Window

The preferred location of Interface Abstraction File System in Widows systems is `c:\itf\`.
In alternative to this, can be in `c:\windows\itf\`.

For non system wide location, can be in user directory, like for example `c:\users\user\itf\`.

The location of Interface Abstraction File System can be inside a specific program, product or other kind of organization direcotory. For example `c:\Program Files\program\itf\`, `c:\Program Files\program\`, `c:\users\user\program\itf\` or `c:\users\user\program\`

### Word Wide Web

The preferred location of Interface Abstraction File System in Word Wide Web systems is `/`.
In alternative to this, can be in `/itf/`. For example `http://www.example.com:80/`, `ftp://example.com/itf/`

The location of Interface Abstraction File System can be inside a specific path or other kind of organization path. For example `127.0.0.1:8888/program/itf/`, `[0:0:0:0:0:0:0:1]:80` or `[::1]/`.

For this specification, we will use preferred location of POSIX systems.

## Names

All names of systems and interfaces use only characters conform ISO 8859-1 Latin 1 (US-ASCII), hexadecimal values:
 - dot, 2E (`.`);

 - number from 30 to 39 (`0`, `1`, `2`, `3`, `4`, `5`, `6`, `7`, `8` and `9`);

 - letters from 41 to 5A (`a`, `b`, `c`, `d`, `e`, `f`, `g`, `h`, `i`, `j`, `k`, `l`, `m`, `n`, `o`, `p`, `q`, `r`, `s`, `t`, `u`, `v`, `w`, `x`, `y` and `z`);

 - letters from 61 to 7A (`A`, `B`, `C`, `D`, `E`, `F`, `G`, `H`, `I`, `J`, `K`, `L`, `M`, `N`, `O`, `P`, `Q`, `R`, `S`, `T`, `U`, `V`, `W`, `X`, `Y` and `Z`);

 - undeline, 5F (`_`).

## Systems

Each external or internal system are a directory inside to `/itf/`. Each subsystem of each system or subsystems are a directory too, inside of respective own system.

## Interfaces

Each interface element are a file. For complex interfaces, each interface complexity level are a directory, creating an interfaces system.

For reresentations of internal system devices, like files in `/proc`, `/sys` or `/dev`, could be from other places, are a link to file.

The interface can be a simple file, link, pipe or executable.

# Support

Open a issue for get support how to implement or for answer your questions.

## Contributing

Feel free to contribute proposing modifications or adaptations throw issues and merge requests.
Any help will aways be welcome and thank you in advance.

## License
This project is licensed under the **Creative Commons Zero v1.0 Universal**.

### No Copyright

The person who associated a work with this deed has dedicated the work to the public domain by waiving all of his or her rights to the work worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.

You can copy, modify, distribute and perform the work, even for commercial purposes, all without asking permission. See Other Information below.

### Other Information
- In no way are the patent or trademark rights of any person affected by CC0, nor are the rights that other persons may have in the work or in how the work is used, such as publicity or privacy rights.

- Unless expressly stated otherwise, the person who associated a work with this deed makes no warranties about the work, and disclaims liability for all uses of the work, to the fullest extent permitted by applicable law.

- When using or citing the work, you should not imply endorsement by the author or the affirmer.
